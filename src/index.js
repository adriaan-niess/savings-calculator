import React from 'react';
import ReactDOM from 'react-dom';
import Chart from "react-apexcharts";
//import 'bootstrap/dist/css/bootstrap.min.css';
import './bootstrap.min.css';
import './index.css';

class SavingsCalculator extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            initialDeposit: 10000,
            initialDepositEntered: 0,
            depositRate: 200,
            interestRate: 3.5,
            investmentLength: 25,
            depositPeriod: 'monthly',
            interestPeriod: 'annually',
            investmentPeriod: 'years',
            depositOrder: 'in advance',
            sumInterest: 0,
            sumDeposits: 0,
            sumTotal: 0,
            chart: {
                options: {
                    chart: {
                        id: "basic-bar",
                    },
                    xaxis: {
                        tickAmount: 10,
                        title: {
                            text: "Month",
                            offsetY: -10
                        }
                    },
                    yaxis: {
                        labels: {
                            formatter: function (value) {
                                return value.toLocaleString('en-US') + "€";
                            }
                        },
                        title: {
                            text: 'Savings'
                        }
                    },
                    legend: {
                        offsetY: 10,
                        height: 30
                    }
                },
                series: [
                    {
                        name: "series-1",
                        data: []
                    }
                ]
            }
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);          
    }

    componentDidMount() {
        this.calculateGrowth();
    }

    investmentPeriodToMonths() {
        if (this.state.investmentPeriod === 'years')
            return 12;
        else
            return 1;
    }

    periodStringToMonths(periodStr) {
        if (periodStr === 'quarterly')
            return 3;
        else if (periodStr === 'semi-annually')
            return 6;
        else if (periodStr === 'annually')
            return 12;
        return 1;
    }

    depositPeriodToMonths() {
        return this.periodStringToMonths(this.state.depositPeriod);
    }

    interestPeriodToMonths() {
        return this.periodStringToMonths(this.state.interestPeriod);
    }

    /** Convert compound interest r with in a periods to b periods */
    convertCompoundRate(a, b, r) {
        return Math.pow(1 + r / b, b / a) - 1;
    }


    handleChange(evt) {
        const name = evt.target.name;
        let value = evt.target.value;
        this.setState({
            [name]: value
        });
    }

    handleSubmit(evt) {
        this.calculateGrowth();
        evt.preventDefault();
    }

    calculateGrowth() {
        // Number of months
        let n = parseInt(this.state.investmentLength, 0) * this.investmentPeriodToMonths();
        
        // Limit max number of months to not exceed memory limits
        n = Math.min(n, 100 * 12)

        // Abort if we have nothing to calculate
        if (n <= 0)
            return;

        // Create table for calculations. Each row:
        // (0) end capital, (1) deposit, (2) interest, (3) sum interest, (4) sum deposits
        let table = Array(n);
        for (let i = 0; i < n; i++) {
            table[i] = Array(5);
            for (let j = 0; j < 5; j++) {
                table[i][j] = 0.0;
            }
        }

        // Initialize deposits
        let months = this.depositPeriodToMonths();
        let firstMonthToPlaceDeposit = this.state.depositOrder === 'in advance' ? 0 : months - 1;
        for (let i = firstMonthToPlaceDeposit; i < n; i += months) {
            table[i][1] = parseFloat(this.state.depositRate);
        }

        // Calculate interest, accumulated interest and end capital
        let interestRate = this.convertCompoundRate(this.interestPeriodToMonths(), 1, parseFloat(this.state.interestRate, 1.0) * 0.01);
        for (let i = 0; i < n; i++) {
            let startCapital = (i <= 0 ? parseFloat(this.state.initialDeposit) : table[i - 1][0]);
            let deposit = table[i][1];
            let interest = 1.0;
            if (this.state.depositOrder === 'in advance') {
                interest = (startCapital + deposit) * interestRate;
            } else {
                interest = startCapital * interestRate;
            }
            let endCapital = startCapital + deposit + interest; 
            let sumInterest = table[i][3] + interest;
            let sumDeposits = table[i][4] + deposit;

            table[i][0] = endCapital;
            table[i][2] = interest;
            table[i][3] = sumInterest;
            table[i][4] = sumDeposits;

            if (i < n - 1) {
                table[i + 1][0] = endCapital;
                table[i + 1][3] = sumInterest;
                table[i + 1][4] = sumDeposits;
            }
        }

        this.setState({
            sumInterest: table[n - 1][3],
            sumTotal: table[n - 1][0],
            sumDeposits: table[n - 1][4],
            initialDepositEntered: parseFloat(this.state.initialDeposit)
        });

        // Update chart
        let chartSumTotal = Array(n);
        let chartSumInterest = Array(n);
        for (let i = 0; i < n; i++) {
            chartSumTotal[i] = parseInt(table[i][0]);
            chartSumInterest[i] = parseInt(table[i][3]);
        }
        let chart = this.state.chart;
        chart.series = [
            {
                name: "Total",
                data: chartSumTotal,
                type: 'line'
            },
            {
                name: "Interest",
                data: chartSumInterest,
                type: 'line'
            }
        ];
        this.setState({
            chart: chart
        });
    }

    render() {
        return (
            <fieldset id="savings-calculator">
                <legend>Savings calculator</legend>
                <form onSubmit={this.handleSubmit}>
                    <table>
                        <tbody>
                            <tr>
                                <td><label>Initial deposit [€]</label></td>
                                <td><input name="initialDeposit" type="text" value={this.state.initialDeposit} onChange={this.handleChange} className="form-control"/></td>
                            </tr>
                            <tr>
                                <td><label>Deposit [€]</label></td>
                                <td><input name="depositRate" type="text" value={this.state.depositRate} onChange={this.handleChange} className="form-control"></input></td>
                                <td>
                                    <select name="depositPeriod" value={this.state.depositPeriod} onChange={this.handleChange} className="form-select">
                                        <option>monthly</option>
                                        <option>quarterly</option>
                                        <option>semi-annually</option>
                                        <option>annually</option>
                                    </select>
                                </td>
                                <td>
                                    <select name="depositOrder" value={this.state.depositOrder} onChange={this.handleChange} className="form-select">
                                        <option>in advance</option>
                                        <option>in arrears</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><label>Interest rate [%]</label></td>
                                <td><input name="interestRate" type="text" value={this.state.interestRate} onChange={this.handleChange} className="form-control"/></td>
                                <td>
                                    <select name="interestPeriod" value={this.state.interestPeriod} onChange={this.handleChange} className="form-select">
                                        <option>monthly</option>
                                        <option>quarterly</option>
                                        <option>semi-annually</option>
                                        <option>annually</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><label>Investment Length &nbsp;</label></td>
                                <td><input name="investmentLength" type="text" value={this.state.investmentLength} onChange={this.handleChange} className="form-control"/></td>
                                <td>
                                    <select name="investmentPeriod" value={this.state.investmentPeriod} onChange={this.handleChange} className="form-select">
                                        <option>years</option>
                                        <option>months</option>
                                    </select>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <input type="submit" value="calculate" className="btn btn-primary"/>
                </form>
                
                <table id="savings-calculator-results">
                    <thead>
                        <tr>
                            <th>Initial deposit</th>
                            <th>Σ Deposits</th>
                            <th>Σ Interest</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{this.state.initialDepositEntered.toLocaleString('en-US')}€</td>
                            <td>{this.state.sumDeposits.toLocaleString('en-US')}€</td>
                            <td>{this.state.sumInterest.toLocaleString('en-US')}€</td>
                            <td>{this.state.sumTotal.toLocaleString('en-US')}€</td>
                        </tr>
                        
                    </tbody>
                </table>
                
                <Chart
                    id="savings-calculator-chart"
                    options={this.state.chart.options}
                    series={this.state.chart.series}
                    width="700px"
                />

            </fieldset>
        );
    }
}

ReactDOM.render(<SavingsCalculator />, document.getElementById('root'));
